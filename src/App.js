import './App.css';
import { Image } from './components/Image/Image';
import { Register } from './components/Register/Register';

function App() {
  return (
    <div className="App">
      <Register />
      <Image />
    </div>
  );
}

export default App;
