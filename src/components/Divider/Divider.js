import './Divider.css';


export const Divider = ({ text }) => {
  return (
    <>
      <div className='divider'>
        <span className='divider_text'>
          {text}
        </span>
      </div>
    </>
  );
}