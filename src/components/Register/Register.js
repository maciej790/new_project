import { Divider } from '../Divider/Divider';
import { GenericButton } from '../GenericButton/GenericButton';
import { GenericInput } from '../GenericInputs/GenericInput';
import { ReactComponent as Google } from '../../common/g logo.svg';
import { ReactComponent as Fb } from '../../common/f logo.svg';
import './Register.css'

export const Register = () => {
  return (
    <div className='wrapper'>
      <div className='register_content'>
        <h1>Register</h1>
        <p className='upperText'>
          Create your own account, and be mathematical.
        </p>
        <GenericButton 
          name='gmail'
          type='button'
          text='Register with Google'
          iconType={<Google />}
        />
        <GenericButton 
          name='fb'
          type='button'
          text='Register with Facebook'
          iconType={<Fb />}
        />
        <Divider 
          text='or use email'
        />
        <GenericInput
          name='email'
          type='email'
          placeholder='Email'
        />
        <GenericInput
          name='password'
          type='password'
          placeholder='Password'
          icon
        />
        <GenericButton 
          name='register_btn'
          type='submit'
          text='Register'
          primary
        />
        <p className='bottomText'>
          Already have an account?&nbsp;
          <a href='/#'>
            Sign in 
          </a>
        </p>   
      </div>
    </div>
  );
}
