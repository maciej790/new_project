import './GenericButton.css';

export const GenericButton = ({ name, type, iconType, text, primary }) => {
  return (
    <>
      <button
        className={primary ? 'genericButton_primary' : 'genericButton_secondary'}
        name={name}
        type={type}
      >
        {/* {iconType &&  */}
          <span className='genericButton_icon'>
            {iconType}
          </span>
        {/* } */}
        {text}
      </button>
    </>
  );
}
