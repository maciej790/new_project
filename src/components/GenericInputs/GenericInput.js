import './GenericInput.css';
import { ReactComponent as Vector } from '../../common/Vector.svg';

export const GenericInput = ({ name, type, placeholder, icon, onChange }) => {
  return (
    <form>
      <input 
        className='genericInput' 
        name={name}
        type={type}
        placeholder={placeholder}
        onChange={onChange}
      />
      {icon && 
        <span className='genericInput_icon'>
         <Vector />
        </span>
      }
    </form>
  );
}
